<div align="center">
  <a href="https://stormgroup.com.br/">
    <img src="https://media.licdn.com/dms/image/C510BAQFwRshHGwkFFA/company-logo_200_200/0/1631353916998?e=2147483647&v=beta&t=M9nalYOZr6bn7yUcmIpcwXkymdmkehXztZ4ShOa9_uY" alt="Storm Group Logo" width="200" height="78"/>
  </a>
</div>

# Storm Group - Desafio FullStack

<div align="center">
  <a href="https:desafio.camargo.dev.br">
    <img src="https://bucketdesafioglobo.s3.amazonaws.com/print_desafio.png" alt="Print da Aplicação" width="200" height="78"/>
  </a>
</div>

## Vídeo Apresentação

[Assista Aqui](https://bucketdesafioglobo.s3.amazonaws.com/Desafio+Globo+-+19+December+2023+(1).mp4)

## Índice

1 - [Leia-me](#leia-me)  
2 - [Implementações](#implementações)  
3 - [Tecnologias e Conceitos](#computer-tecnologias-e-conceitos)  
4 - [Acessos](#computer-acessos)  
5 - [Rodando a aplicação](#-rodando-a-aplicação)

## Leia-me

Olá equipe Storm e Equipe Globo, como estão? Em resposta ao desafio que me foi proposto, desenvolvi esta Aplicação.

Aproveitando o tempo que me foi disponibilizado, optei por hospedar a API na Google Cloud e vocês podem acessá-la clicando neste link: [Challenge-Storm-GCP](https://desafio.camargo.dev.br/). A API foi documentada seguindo as especificações OpenAPI, portanto, vocês podem utilizar o swagger para testar a API, disponível aqui: [Documentação](https://desafio.camargo.dev.br/api/docs/).

Gostaria de agradecer pela oportunidade de participar deste processo seletivo e estou sempre disposto a receber feedbacks e críticas construtivas tanto sobre o processo seletivo quanto sobre o meu código. Acredito que a troca de experiências e ideias é fundamental para o crescimento pessoal e profissional.

Obrigado novamente pela oportunidade.

## Implementações

-   API Rest desenvolvida com Node + Express;
-   API documentada com swagger - documentação pode ser acessada pelo link http://localhost:<API_PORT>/docs/
-   API Dockerizada
-   Front Web desenvolvido com React + Vite;
-   Front Web Dockerizado

A API possibilita:  
✅ Registro e autenticação de usuário  
✅ Criação, atualização, Ativação/Desativação de um Filme  
✅ Listagem de Filmes, incluindo filtragem por Nome do Diretor, Id do Ator, Id Do Genero, Busca Elástica por título  
✅ Listagem de Diretores  
✅ Listagem de Gêneros  
✅ Listagem de Atores  
✅ Upload de arquivos, por meio de rota dedicada e autenticada  
✅ Avaliação de um filme

O Front End possibilita:  
✅ Listagem de Filmes, incluindo filtragem por Nome do Diretor, Id do Ator, Id Do Genero, Busca Elástica por título  
✅ Listagem de Filmes por Gênero  
✅ Detalhes de um Filmes, incluindo filmes relacionados  
✅ Avaliar um Filme  
✅ Autenticação  
✅ Cadastro/Edição/Ativação/Desativação de Usuário  
✅ Cadastro/Edição de Filme

## :computer: Tecnologias e Conceitos

-   Node
-   TypeORM
-   Yup
-   MySQL
-   Passport
-   Swagger
-   Docker
-   GCP
-   React
-   Vite

---

## :computer: Acessos e Pontos importantes

**Acesso Administrador**

```
 Email: admin@imdb.com.br
 Senha: Adm@123_456
```

-   Para acessar a área administrativa, basta clicar no botão "Área do Administrador" após realizar o login.

-   Para cadastrar um novo Ator, no campo 'Atores', basta digitar o nome do ator e clicar no botão 'Adicionar'. Caso o ator não exista, ele será criado automaticamente.

## 🏁 Rodando a aplicação

Certifique-se que voce possui [Docker](https://www.docker.com/) e todas suas dependências rodando localmente. Durante o desenvolvimento foi utilizada a versão 20.10.12.

Primeiro, faça o clone desse repositório na sua maquina:

```

git clone --recursive https://gitlab.com/camargo_challenge/imdb_monorepo.git

```

O segundo passo é, dentro da pasta raiz do backend, substituir o nome do arquivo '.env.example' para '.env'. Preencha todas as variáveis que estão vazias (_Não recomendo a alteração de variáveis caso elas já estejam preenchidas_ ). Certifique-se de que as portas indicadas no .env estão disponiveis e que o apontamento está sendo feito para o container do banco de dados.

Depois, dentro da pasta raiz do backend, rode o seguinte comando para iniciar o container. Não recomendo a utilização do script disponivel no package.json para inicialização do docker.

```
docker-compose up --build
```

**Repita o segundo passo dentro da pasta raiz do frontend.**

**⚠️ Pontos importantes ⚠️ -**

-   O Front-end deve apontar para o backend (http://localhost:<PORT>) sem nenhum prefixo.

-   Para garantir o funcionamento correto da sua aplicação, é importante acompanhar os logs do container. Se a aplicação tentar se conectar ao banco de dados antes dele estar pronto, pode ocorrer um erro. Neste caso, a aplicação tentará se conectar novamente em alguns segundos. Caso o erro persista, verifique o apontamento para o banco de dados no arquivo .env e certifique-se de que as migrations estão sendo executadas automaticamente. Se necessário, execute o comando da migration no bash do container.

Pronto! A aplicação estará rodando na porta 3000.

## 🤔 Das escolhas

-   Além das tecnologias e conceitos solicitados, fiz algumas escolhas que seguem abaixo:

-   **TypeORM** - Optei por utilizar o TypeORM para facilitar a criação das migrations e a conexão com o banco de dados. Além disso, o TypeORM possui uma integração com o Swagger que facilita a documentação da API. Busquei utilizar o mesmo ORM da empresa, porém, não consegui encontrar nas minhas pesquisas.

-   **Yup** - Utilizei o Yup para validação dos dados de entrada. Acredito que a utilização de um validador de dados é fundamental para garantir a integridade dos dados e evitar erros no banco de dados. Porém, após o ínicio do desenvolvimento, percebi que deveria ter utilizado o Zod, que é o que estou mais acostumado a utilizar.

-   **Passport** - Utilizei o Passport para autenticação da API. Acredito que o Passport é uma ferramenta muito poderosa e que facilita muito a implementação de autenticação em uma API. Além disso, o Passport possui uma integração com o Swagger que facilita a documentação da API.

-   **Swagger** - Utilizei o Swagger para documentar a API. Um dos requisitos do desafio era a documentação da API, por ter sido citado a especificação OpenAPI, optei por utilizar o Swagger.

-   **Docker** - Utilizei o Docker para facilitar a execução da aplicação. Tanto o backend quanto o frontend estão dockerizados, porém a intenção era dockerizar tudo em um único compose. Tive alguns problemas na inicialização da API e por priorização, optei por deixar o frontend e o backend separados.

-   **GCP** - Utilizei as instâncias da GCP para hospedar a API. Isso foi tratado como prioridade durante o meu desenvolvimento, para que vocês pudessem acessar a aplicação sem precisar rodar localmente. A Aplicação está sendo executada em uma E2-Micro, com 2 vCPUs e 2GB de memória, sendo mais que o suficiente para a aplicação.

-   **Vite** - Optei por utilizar o Vite para criar o frontend. Acredito que o Vite é uma ferramenta muito poderosa e que facilita muito o desenvolvimento de aplicações React. Além disso, o Vite possui uma integração com o Typescript que facilita muito o desenvolvimento.

-   **Radix & Shadcn** - Utilizei o Radix e o Shadcn para estilizar o frontend. Acredito que o Radix e o Shadcn são ferramentas muito modernas e que facilitam muito o desenvolvimento de aplicações React.

-   **Axios** - Utilizei o Axios para realizar as requisições HTTP. Pensei inicialmente em utilizar React Query, porém, dado o contexto do desafio, optei por utilizar o Axios.

## 🤔 O que faria a seguir

-   **Testes** - Acredito que a implementação de testes é fundamental para garantir a integridade da aplicação. Na minha priorização, dedicaria um tempo para implementar testes de integração e E2E.

-   **CI/CD** - Acredito que a implementação de CI/CD é fundamental para garantir a integridade da aplicação. Na minha priorização, essa seria a próxima etapa do desenvolvimento.

## 📝 Licença

Projeto para fins de desafio técnico. O compartilhamento e utilização do código é livre.
